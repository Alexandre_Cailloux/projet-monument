﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    Animator animator;
    int levelToLoad;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void FadeToLevel(int levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
    }

    public void Fade()
    {
        levelToLoad = 0;
        animator.ResetTrigger("FadeOut");
        animator.SetTrigger("FadeOut");
    }

    public void Increase()
    {
        levelToLoad = 0;
        animator.ResetTrigger("FadeOut");
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeComplete()
    {
        if (levelToLoad != 0)
            SceneManager.LoadScene(levelToLoad);
    }
}
