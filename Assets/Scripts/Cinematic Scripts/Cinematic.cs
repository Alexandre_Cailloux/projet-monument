﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cinematic : MonoBehaviour
{
    [SerializeField] GameObject image1;
    [SerializeField] GameObject image2;
    [SerializeField] GameObject image3;
    [SerializeField] LevelChanger levelChanger;

    [SerializeField] GameObject text1;
    [SerializeField] GameObject text2;
    [SerializeField] GameObject text3;
    [SerializeField] GameObject text4;

    void Start()
    {
        StartCoroutine(CinematicCo());
    }

    IEnumerator CinematicCo()
    {
        FindObjectOfType<AudioManager>().Play("cinematic");
        yield return new WaitForSeconds(9f);
        text1.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        text2.SetActive(true);
        yield return new WaitForSeconds(9f);
        text2.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        levelChanger.Fade();
        yield return new WaitForSeconds(1f);
        image1.SetActive(false);
        image2.SetActive(true);
        levelChanger.Increase();
        yield return new WaitForSeconds(0.3f);
        text3.SetActive(true);
        yield return new WaitForSeconds(6f);
        text3.SetActive(false);
        yield return new WaitForSeconds(0.3f); 
        levelChanger.Fade();
        yield return new WaitForSeconds(1f);
        image2.SetActive(false);
        image3.SetActive(true);
        levelChanger.Increase();
        yield return new WaitForSeconds(0.3f); 
        text4.SetActive(true);
        yield return new WaitForSeconds(6f);
        text4.SetActive(false);
        yield return new WaitForSeconds(0.3f); 
        levelChanger.FadeToLevel(2);
        FindObjectOfType<AudioManager>().Stop("cinematic");  
    }
}
