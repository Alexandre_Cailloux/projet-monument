
Année : 2019-2020
Création du GIT : 26/09/2019
Lieu d'étude : IUT Informatique d'Orléans
Classe : 2A31

Dans le cadre de notre formation universitaire en DUT Informatique,
nous devons réaliser un projet de conception d'un jeu en anglais,
ou l'on fait référence à des monuments à destination d'intervenants
étrangés.


Membres :
 - Clément DORMIGNIES
 - Alexandre CAILLOUX
 - Tristan CHAUVIN
 - Hugo PREVOT
 - Jonas PERIBANEZ
 - Diego PERIBANEZ

Liens :
 - Drive du projet : https://drive.google.com/drive/folders/17RMesOZXEAuZLKKDIOfQOaP-d2Hl4lKt